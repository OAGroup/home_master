/**
  ******************************************************************************
  * @file    stm32f2xx_it.c
  * @author  MCD Application Team
  * @version V1.0.2
  * @date    06-June-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

#include "stm32f2xx_it.h"
#include "mxchipWNET.h"



//
void NMI_Handler(void){
}

__asm void asm_dump(void)
{
	 IMPORT hard_fault_handler_c
	 TST LR, #4
	 ITE EQ
	 MRSEQ R0, MSP
	 MRSNE R0, PSP
	 B hard_fault_handler_c
 }

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void){   
	system_reload();
	while(1){
	}	
	//asm_dump();
}


//
void MemManage_Handler(void){
  while (1){
  }
}

//
void BusFault_Handler(void){
  while (1){
  }
}


void UsageFault_Handler(void){
  while (1){
  }
}


void DebugMon_Handler(void){
}



#include	"cmsis_os.h"

void SysTick_Handler(void){
	systick_irq();    
	NoOS_systick_irq();
	osSystickHandler();
}


void EXTI15_10_IRQHandler(void)
{
  gpio_irq();
}

//
void EXTI0_IRQHandler(void){
	gpio_irq(); //SDIO OOB interrupt
}

//
//void EXTI3_IRQHandler(void)
//{
//	gpio_irq(); //User defined external interrupt, EMW3162 button 1: PA3
//}

//
void SDIO_IRQHandler(void){
  sdio_irq();
}

//
void DMA2_Stream3_IRQHandler(void){ 
	dma_irq();
}


