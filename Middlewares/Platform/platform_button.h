/**
*
* @brief	AirKiss 按键
* @author	yun
* @date		2016-12-23
* @desc		AirKiss 按键
* @version
*
*/


#ifndef		__PLATFORM_BUTTON_H
#define		__PLATFORM_BUTTON_H

typedef	int (*platform_airkiss_callback)(void);

int platform_airkiss_btn(platform_airkiss_callback callback);

int platform_button_init(void);

int platform_button_test(void);

#endif
