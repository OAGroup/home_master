/**
* 
* @brief	433模块驱动
* @author	yun
* @date		2016-12-22 
* @desc		433模块，控制开关和室内灯
* @version	V0.1
*
*/

#ifndef		__PLATFORM_433MODULE_H
#define		__PLATFORM_433MODULE_H

typedef	 void(*	module433_callback_fn)(char * buffer, int len);

int platform_module433_init(void);

int platform_module433_notification(module433_callback_fn callback);

int platform_module433_send(unsigned char *buffer, int num);

int platform_module433_test(void);

#endif


