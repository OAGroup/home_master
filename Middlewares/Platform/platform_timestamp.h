/***
*
* @brief		timestamp
* @author		yun
* @date			2016-12-20 11:01:16
* @version		1.0.1
* @desc			
*
*/

#ifndef		__PLATFORM_TIMESTAMP_H
#define		__PLATFORM_TIMESTAMP_H

extern int mcu_timestamp_init(void);
extern unsigned int mcu_timestamp_get(void);

#endif

