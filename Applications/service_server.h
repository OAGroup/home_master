/**
*
* @brief	服务器连接状态管理服务
* @author	yun
* @date		2016-12-21
* @desc     创建与销毁服务器连接线程，处理设备与服务器数据连接
* @version                                          
*		
* 
**/

#ifndef		__SERVICE_SERVER_H
#define		__SERVICE_SERVER_H

#include	"cJSON.h"

int app_server_init(void);

int app_server_establish(void);
int app_server_destroy(void);

typedef	int (* server_post_callback_fn)(char * sev, cJSON * data);

int server_send_data(char * service, char *data);

int server_post_subscribe(char * service, server_post_callback_fn post_callback);

#endif


