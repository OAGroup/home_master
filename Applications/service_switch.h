/**
*
* @brief	开关控制服务
* @author	yun
* @date		2016-12-23
* @desc		(433模块控制卧室灯、台灯、厨房灯等设备)
* @version
*
*
*/

#ifndef		__SERVICE_SWITCH_H
#define		__SERVICE_SWITCH_H


int service_switch_establish(void);

int service_switch_destroy(void);

int switch_state_query();

int switch_operate_cmd(char *cmd);

#endif


