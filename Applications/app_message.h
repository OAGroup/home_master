/**
*
* @brief	消息格式定义
* @author	yun
* @date		2016-12-21
* @desc		与服务器交换数据格式
* @version  v0.1
*
*/

#ifndef		__APP_MESSAGE_H
#define		__APP_MESSAGE_H


#include	"cJSON.h"

typedef	struct {
	char *  	type;
	char *  	date; 
}message_send_t;

typedef struct {
	char *  	type;
	cJSON *		data;
	cJSON *		json;
} message_post_t;

int message_encode(char * packet, const message_send_t * send);
int message_post_parse(message_post_t * post, const char * packet);
int message_post_release(message_post_t * post);

int message_test(void);

#endif
