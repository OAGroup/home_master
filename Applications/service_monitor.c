/**
*
* @brief	系统运行信息检测统计服务
* @author	yun
* @date		2017-02-07
* @desc		统计FreeRTOS运行堆栈，CPU利用率等信息
* 			管理日志输出功能
* @version	v0.1
*
*/

#include	"service_monitor.h"

#include	"platform_logger.h"

#include	<string.h>
#include	<stdio.h>

#define		len(arr)	(sizeof(arr)/sizeof(arr[0]))

//
// 监控日志及其对于的字符串命令
//
const char * monitor_cmd_arr[] = {
	[PLATFORM_LOG_DEBUG] = "log-debug",
	[PLATFORM_LOG_INFO] = "log-info",
	[PLATFORM_LOG_NOTICE] = "log-notice",
	[PLATFORM_LOG_WARNING] = "log-warning",
	[PLATFORM_LOG_ERROR] = "log-error",
	[PLATFORM_LOG_OFF] = "log-off"
};


//
// 监控统计信息命令行控制
//
int service_monitor_cmd(char * cmd){
	if (cmd == NULL) {
		return -1;
	}
	
	int index = 0;
	
	for(; index<len(monitor_cmd_arr); index++){
		if(strcmp(cmd, monitor_cmd_arr[index]) == 0){
			break;
		}
	}
	
	if (index <= PLATFORM_LOG_OFF) {
		platform_log_set_level(index);
		return 0;
	}
	
	return 0;
}




